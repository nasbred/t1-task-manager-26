package ru.t1.kharitonova.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect.");
    }

    public NumberIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This value is incorrect: \"" + value + "\"", cause);
    }

    public NumberIncorrectException(@NotNull final String value) {
        super("Error! This value is incorrect: \"" + value + "\"");
    }

}
