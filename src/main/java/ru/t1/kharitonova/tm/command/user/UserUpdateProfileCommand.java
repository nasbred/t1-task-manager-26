package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update current user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-update-profile";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter First Name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter Last Name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
