package ru.t1.kharitonova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.util.FormatUtil;

public final class SystemInfoCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        @NotNull final int availableProcessors = Runtime.getRuntime().availableProcessors();
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        @NotNull final String maximumMemory = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final long usageMemory = totalMemory - freeMemory;

        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maximumMemory);
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display system information.";
    }

}
