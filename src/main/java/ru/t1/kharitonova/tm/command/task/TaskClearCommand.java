package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Clear tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        @NotNull final String userId = getUserId();
        getTaskService().removeAllByUserId(userId);
    }

}
