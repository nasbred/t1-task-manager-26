package ru.t1.kharitonova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of terminal commands.";
    }

}
