package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.TaskSort;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Display list tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks;
        if (sort == null) tasks = getTaskService().findAll(userId);
        else tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName() + " : " + task.getStatus());
            index++;
        }
    }

}
